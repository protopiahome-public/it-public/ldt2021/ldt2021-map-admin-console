import React, {Component, Fragment} from "react";
import ReactPaginate from 'react-paginate';

export default class Pagi extends Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			all		: props.all,
			current	: props.current
		};
	}
	componentWillReceiveProps ( nextProps )
	{
		//console.log( nextProps );
		this.setState({
			all		: nextProps.all,
			current	: nextProps.current
		});
	}
	render()
	{
		const {all, current} = this.state;
		return <Fragment>
			<ReactPaginate
				previousLabel={'<'}
				nextLabel={'>'}
				breakLabel={'...'}
				breakClassName={'break-me'}
				pageCount={all}
				marginPagesDisplayed={3}
				pageRangeDisplayed={6}
				onPageChange={data => this.props.onChoose( data.selected )}
				containerClassName={'pagination pagination-sm'}
				pageClassName={"page-item"}
				pageLinkClassName={"page-link"}
				previousClassName={"page-item"}
				previousLinkClassName={"page-link"}
				nextClassName={" page-item"}
				nextLinkClassName={"page-link"}
				breakClassName={"page-item desabled"}
				breakLinkClassName={"page-link"}
				activeClassName={'active'}
				forcePage={current}
			/>
		</Fragment>
	}
	render2()
	{
		const {all, current} = this.state;
		let btns = [];
		for(let i=0; i<all ; i++)
		{
			let ii = i+1;
			const cls = i == current ? " page-item active " : " page-item ";
			// console.log(cls);
			btns.push(   <li className={cls} key={i}>
					<div className="page-link" onClick={this.onChoose} data-i={i}>{ii} </div>
				</li>
			);
		}
		return <ul className="pagination pagination-sm">
			{btns}
		</ul>
	}
	onChoose = evt =>
	{
		const i = parseInt(evt.currentTarget.getAttribute("data-i"));
		this.setState({current: i});
		this.props.onChoose(i);
	}
}
//https://github.com/AdeleD/react-paginate/blob/master/demo/js/demo.js