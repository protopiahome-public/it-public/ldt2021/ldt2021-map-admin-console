import React from "react";
import BasicState from "./BasicState";

//import {useMutation} from '@apollo/react-hooks';

import { compose } from "recompose";
import {withApollo} from 'react-apollo';
import {withRouter} from "react-router";
import gql from "graphql-tag";

class UploadFileState extends BasicState
{
	upload = ({
		target: {
		  validity,
		  files: [file]
		}
	  }) => {
		  this.props.client.mutate({
			  mutation: gql`
				mutation ($input: FileInput) {
					uploadFile(input: $input)
				}
			  `,
			  variables: {
				  input: {
					  file_upload: file
				  }
			  }
		  })
	  }

	myState = () =>
	{

		return <>
			<input type="file" onChange={this.upload}/>
		</>;
	}
	getRoute = () =>
	{
		return "upload";
	}
}
export default compose(
	withApollo,
	withRouter
)(UploadFileState);