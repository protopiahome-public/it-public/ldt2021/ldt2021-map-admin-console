import React, {Component} from "react";
import {__} from "../../../utilities/i18n"
import Form from "./Form";
import TextEditor from "../../../utilities/TextEditor";
import { Tag, ButtonGroup, Button, Intent, TextArea } from "@blueprintjs/core";

export default class TextField extends Form
{
	isEnabled()
	{
		const {field, title} = this.props;
		const {value} = this.state;
		if (this.props.apollo_field && this.props.apollo_field.variant == 'wysiwyg') {
			return <TextEditor onChange={this.onChange} text={this.state.value}/>;
		}
		return <TextArea
				value={ value ? value : ""}
				onChange={this.onChange}
				className="form-control "
			/>;
	}
	isDesabled()
	{
		const {field, title} = this.props;
		const {value} = this.state;
		return <div className="px-0 my-2">
			{
				this.props.value 
					?
					<Tag minimal={true}>
						{ this.props.value + " "}
					</Tag>
					:
					null
			}
		</div>
	}
	
	onChange = evt =>
	{
		this.setState({value: this.props.apollo_field && this.props.apollo_field.variant == 'wysiwyg' ? evt : evt.currentTarget.value});
		this.on(this.props.apollo_field && this.props.apollo_field.variant == 'wysiwyg' ? evt : evt.currentTarget.value)
	}
	
	on = value =>
	{
		this.props.on( value, this.props.field, this.props.title );
	}
}